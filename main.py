# standard libraries
import machine
import time
import ujson
import uerrno
from umqtt.simple import MQTTClient
from micropython import const

# custom / user libraries
import bme280 # https://github.com/catdog2/mpy_bme280_esp8266

# constants
IDLETIME = const(10000) # 10 seconds
DEEPSLEEPTIME = const(600000) # 10 minutes
WEMOSD1MINI_PIND1 = const(5) # wemos d1 mini i2c scl
WEMOSD1MINI_PIND2 = const(4) # wemos d1 mini i2c sda

# define function for putting esp8266 in deep sleep
def GoDeepSleep(msecs):
    # configure RTC.ALARM0 to be able to wake the device
    rtc = machine.RTC()
    rtc.irq(trigger=rtc.ALARM0, wake=machine.DEEPSLEEP)

    # set RTC.ALARM0 to fire after X milliseconds (waking the device)
    rtc.alarm(rtc.ALARM0, msecs)

    print("Going into deep sleep...")

    # put the device in deep sleep
    machine.deepsleep()

# retrieve per device configuration
#
# config.json schema:
"""
{
	"ssId": "<SSID of WLAN>",
	"key": "<Password / Network Key>",
	"mqttServer": "<FQDN or IP address of MQTT broker>",
	"mqttPort": <MQTT server port number>,
	"deviceId": "<device ID of MQTT client>"
}
"""
t0 = time.ticks_us()

try:
    with open("config.json", "r") as configFile:
        configData = ujson.load(configFile)
except OSError as e: # config.json is missing
	print("Config file not readable:", uerrno.errorcode[e.args[0]])

# test for all key-value pairs
try:
    print("ssID:", configData["ssId"])
    print("key:", configData["key"])
    print("mqttServer:", configData["mqttServer"])
    print("mqttPort:", configData["mqttPort"])
    print("sensors/deviceId:", configData["deviceId"])
except KeyError as e: # key-value pair is missing
	print("Key-Value pair missing:", e.args[0])

# connect to wifi
try:
	ipAddr = WiFiConnect(configData["ssId"], configData["key"])
except NameError as e: # config.json is missing
	print("WiFi failed to connect:", e.args[0])
except KeyError as e: # key-value pair is missing
	pass # error msg printed in prior try block

try:
    # setup connection to mqtt broker
	c = MQTTClient(client_id=configData["deviceId"], server=configData["mqttServer"], port=configData["mqttPort"])
	c.connect()
	print("MQTT broker connected...")
except NameError as e: # config.json is missing
	print("MQTT broker failed to connect:", e.args[0])
except KeyError as e: # key-value pair is missing
	pass # error msg printed in prior try block
except OSError as e: # mqtt broker is not reachable
	print("MQTT broker failed to connect:", uerrno.errorcode[e.args[0]])

try:
	# setup comms to sensor
	i2c = machine.I2C(scl=machine.Pin(WEMOSD1MINI_PIND1), sda=machine.Pin(WEMOSD1MINI_PIND2))
	bme = bme280.BME280(i2c=i2c)

	# read sensor data
	data = bme.values
	print("(T,P,H):", data)
except OSError as e: # shield not attached or HW failure
	print("Sensor failed to communicate:", uerrno.errorcode[e.args[0]])

try:
    # publish data to mqtt broker
    c.publish("sensors/"+configData["deviceId"]+"/temperature", data[0])
    c.publish("sensors/"+configData["deviceId"]+"/pressure", data[1])
    c.publish("sensors/"+configData["deviceId"]+"/humidity", data[2])
    c.publish("sensors/"+configData["deviceId"]+"/ip-address", ipAddr)

    # disconnect from mqtt broker
    c.disconnect()
    print("Sensor readings published...")
except NameError as e: # config.json is missing
    print("Sensor readings failed to publish:", e.args[0])
except KeyError as e: # key-value pair is missing
	pass # error msg printed in prior try block
except OSError as e: # mqtt broker is not reachable
	print("Sensor readings failed to publish:", uerrno.errorcode[e.args[0]])

t1 = time.ticks_us()
dt = time.ticks_diff(t1, t0)
fmt = 'Power on-to-publish Time: {:5.3f} sec'
print(fmt.format(dt * 1e-6))

# idle for short time
time.sleep_ms(IDLETIME)

# deep sleep to save current & mitigate erroneous temp reading from self-heating
GoDeepSleep(DEEPSLEEPTIME)
