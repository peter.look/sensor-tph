# This file is executed on every boot (including wake-boot from deepsleep)

# standard libraries
import gc

gc.collect()

print("Power on from reset...")

# define function for connecting to WiFi
def WiFiConnect(ssid, passwd):
    # Based originally on https://goo.gl/5UMJDr
    import network
    sta_if = network.WLAN(network.STA_IF)
    if not sta_if.isconnected():
        print('Connecting to WiFi...')
        sta_if.active(True)
        sta_if.connect(ssid, passwd)
        while not sta_if.isconnected():
            # if stuck in this loop, confirm ssid / passwd values
            pass
    ipconfig = sta_if.ifconfig()
    print("WiFi connected...")
    print("IP config:", ipconfig)
    return ipconfig[0]
